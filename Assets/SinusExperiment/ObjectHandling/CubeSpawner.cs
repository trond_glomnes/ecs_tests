﻿using System.Collections.Generic;
using EcsPure;
using SinusExperiment.EcsPure;
using UnityEngine;

namespace Mover
{
	public class CubeSpawner : MonoBehaviour
	{
		[SerializeField] public int Amount;
		[SerializeField] public GameObject CubePrefab;

		private List<GameObject> cubes = new List<GameObject>();

		private void Awake()
		{
			SpawnCubes(Amount);
		}

		public void Respawn(int amount)
		{
			DestroyCubes();
			SpawnCubes(amount);
		}

		private void DestroyCubes()
		{
			foreach (var cube in cubes)
			{
				Destroy(cube);
			}
		}

		private void SpawnCubes(int amount)
		{
			for (int i = 0; i < amount; i++)
			{
				var cube = Instantiate(CubePrefab);
				cube.transform.parent = transform;
				cube.transform.localPosition = new Vector3(i, 0, 0);
				cubes.Add(cube);

				var wrapper = cube.GetComponent<SinusSpeedWrapper>();
				if (wrapper != null)
				{
					wrapper.Value = new SinusSpeed {Value = i};
				}
			}
		}
	}
}