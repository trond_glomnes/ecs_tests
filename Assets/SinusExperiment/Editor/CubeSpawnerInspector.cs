﻿using Mover;
using UnityEditor;
using UnityEngine;

namespace Editor
{
	[CustomEditor(typeof(CubeSpawner))]
	public class CubeSpawnerInspector : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			var spawner = target as CubeSpawner;

			// ReSharper disable once PossibleNullReferenceException
			spawner.CubePrefab =
				(GameObject) EditorGUILayout.ObjectField("Prefab", spawner.CubePrefab, typeof(GameObject), false);
			
			var newAmount = EditorGUILayout.DelayedIntField(spawner.Amount);
			if (EditorApplication.isPlaying && newAmount != spawner.Amount)
			{
				spawner.Respawn(newAmount);
			}

			spawner.Amount = newAmount;

			if (!EditorApplication.isPlaying)
			{
				EditorUtility.SetDirty(target);
			}
		}
	}
}