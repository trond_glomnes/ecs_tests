﻿using System;
using JetBrains.Annotations;
using Unity.Entities;

namespace SinusExperiment.EcsPure
{
	[Serializable]
	public struct SinusSpeed:IComponentData
	{
		[UsedImplicitly] public float Value;
	}
}