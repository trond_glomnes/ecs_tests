﻿using JetBrains.Annotations;
using SinusExperiment.EcsPure;
using Unity.Entities;
using Unity.Jobs;
using UnityEngine;

namespace EcsPure
{
	[UsedImplicitly]
	public class SinusMovementJobSystem:JobComponentSystem
	{
		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			var job = new SinusMovementJob(){Time = Time.deltaTime+Time.timeSinceLevelLoad};
			return job.Schedule(this, 64, inputDeps);
		}
	}
}