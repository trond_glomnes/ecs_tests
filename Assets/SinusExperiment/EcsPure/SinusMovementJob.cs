﻿using System;
using EcsPure;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;

namespace SinusExperiment.EcsPure
{
	[ComputeJobOptimization]
	[Serializable]
	internal struct SinusMovementJob : IJobProcessComponentData<Position,SinusSpeed>
	{
		public float Time { set; private get; }
			
		public void Execute(ref Position position, ref SinusSpeed sinusSpeed)
		{
			position.Value.z = Mathf.Sin((Time * sinusSpeed.Value) + position.Value.x);
		}
	}
}