﻿using Mover;
using UnityEngine;

namespace ECS.SinusMovement
{
	public struct SinusMovementData
	{
		public Transform Transform;
		public SinusCube Cube;
	}
}