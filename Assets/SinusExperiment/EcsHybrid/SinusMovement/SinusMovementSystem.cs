﻿using ECS.SinusMovement;
using JetBrains.Annotations;
using Unity.Entities;
using UnityEngine;

namespace EcsHybrid.SinusMovement
{
	[UsedImplicitly]
	public class SinusMovementSystem : ComponentSystem
	{
		//[Inject] private SinusMovementData _data;

		protected override void OnUpdate()
		{
			var time = Time.deltaTime+Time.timeSinceLevelLoad;

			ComponentGroupArray<SinusMovementData> entities = GetEntities<SinusMovementData>();
			foreach (var entity in entities)
			{
				var position = entity.Transform.localPosition;
				position.z = Mathf.Sin(time + position.x);
				entity.Transform.localPosition = position;
			}

		}
	}
}
