﻿using System;
using JetBrains.Annotations;
using Unity.Mathematics;
using Unity.Transforms;
using VoxelWorld.Core.Interfaces;

namespace VoxelWorld.Core.PositionStructure
{
	public struct VoxelPosition
	{
		public GlobalPosition GlobalPosition { get; }
		public readonly int3 LocalPosition;
		
		public IChunk Chunk { get; }

		public int X
		{
			get { return LocalPosition.x; }
		}

		public int Y
		{
			get { return LocalPosition.y; }
		}

		public int Z
		{
			get { return LocalPosition.z; }
		}


		public VoxelPosition(int3 localPosition, [NotNull] IChunk chunk)
		{
			if (chunk == null) throw new ArgumentNullException(nameof(chunk));

			Chunk = chunk;
			LocalPosition = localPosition;
			GlobalPosition = new int3(LocalPosition.x % Chunk.chunkSize, LocalPosition.y % Chunk.chunkSize,
				LocalPosition.z % Chunk.chunkSize);
			
			Chunk = chunk;
			LocalPosition = new int3(GlobalPosition.X % Chunk.chunkSize, GlobalPosition.Y % Chunk.chunkSize,
				GlobalPosition.Z % Chunk.chunkSize);
			
			if (!IsPositionInRange(localPosition))
				throw new ArgumentOutOfRangeException("Position is outside the borders of the chunk!");
		}

		public VoxelPosition(int localX, int localY, int localZ, [NotNull] IChunk chunk)
		{
			if (chunk == null) throw new ArgumentNullException(nameof(chunk));

			Chunk = chunk;
			LocalPosition = new int3(localX, localY, localZ);
			GlobalPosition = new int3(LocalPosition.x % Chunk.chunkSize, LocalPosition.y % Chunk.chunkSize,
				LocalPosition.z % Chunk.chunkSize);
			
			if (!IsPositionInRange(LocalPosition))
				throw new ArgumentOutOfRangeException("Position is outside the borders of the chunk!");
		}

		//TODO remove
		/*public bool IsPositionInRange(int3 localPosition)
		{
			var globalPos = Chunk.GlobalPosition;

			return localPosition.x >= globalPos.X && localPosition.x < globalPos.X + Chunk.chunkSize &&
			       localPosition.Y >= globalPos.Y && localPosition.Y < globalPos.Y + Chunk.chunkSize &&
			       localPosition.Z >= globalPos.Z && localPosition.Z < globalPos.X + Chunk.chunkSize;
		}*/
		
		public bool IsPositionInRange(int3 localPosition)
		{
			return localPosition.x >= 0 && localPosition.x < Chunk.chunkSize &&
			       localPosition.y >= 0 && localPosition.y < Chunk.chunkSize &&
			       localPosition.z >= 0 && localPosition.z < Chunk.chunkSize;
		}

		public static VoxelPosition operator +(VoxelPosition voxelPosition, int3 int3)
		{
			return new VoxelPosition(voxelPosition.X + int3.x, voxelPosition.Y + int3.y, voxelPosition.Z + int3.z,
				voxelPosition.Chunk);
		}
	}
}