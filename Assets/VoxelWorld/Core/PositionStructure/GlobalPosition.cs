﻿using Unity.Mathematics;

namespace VoxelWorld.Core.PositionStructure
{
	public struct GlobalPosition
	{
		public int3 Value { get; }

		public int X
		{
			get { return Value.x; }
		}
		
		public int Y
		{
			get { return Value.y; }
		}
		
		public int Z
		{
			get { return Value.z; }
		}

		public GlobalPosition(int3 globalPosition)
		{
			Value = globalPosition;
		}

		public GlobalPosition(int x, int y, int z)
		{
			Value = new int3(x,y,z);
		}

		public static implicit operator GlobalPosition(int3 position)
		{
			return new GlobalPosition(position);
		}

		public override string ToString()
		{
			return Value.ToString();
		}
	}
}