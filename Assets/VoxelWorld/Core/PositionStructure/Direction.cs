﻿using Unity.Mathematics;

namespace VoxelWorld.Core.PositionStructure
{
	public struct Direction
	{
		public static readonly Direction Up = new Direction(new int3(0,1,0));
		public static readonly Direction Down = new Direction(new int3(0,-1,0));
		public static readonly Direction Left = new Direction(new int3(-1,0,0));
		public static readonly Direction Right = new Direction(new int3(1,0,0));
		public static readonly Direction Front = new Direction(new int3(0,0,1));
		public static readonly Direction Back = new Direction(new int3(0,0,-1));
		
		private readonly int3 _direction;

		private Direction(int3 direction)
		{
			_direction = direction;
		}

		public static implicit operator int3(Direction direction)
		{
			return direction._direction;
		}
	}
}