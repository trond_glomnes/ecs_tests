﻿using Unity.Mathematics;

namespace VoxelWorld.Core.PositionStructure
{
	public struct ChunkPosition
	{
		public int3 LocalPosition { get; }
		public readonly int ChunkSize;

		public int X
		{
			get { return LocalPosition.x; }
		}
		
		public int Y
		{
			get { return LocalPosition.y; }
		}
		
		public int Z
		{
			get { return LocalPosition.z; }
		}
		
		public int3 GlobalPosition
		{
			get
			{
				return new int3()
				{
					x = LocalPosition.x*ChunkSize,
					y = LocalPosition.y*ChunkSize,
					z = LocalPosition.z*ChunkSize,
				};
			}
		}

		public ChunkPosition(GlobalPosition globalPosition, int chunkSize)
		{
			ChunkSize = chunkSize;
			LocalPosition = new int3()
			{
				x = globalPosition.X/chunkSize,
				y = globalPosition.Y/chunkSize,
				z = globalPosition.Z/chunkSize,
			};
		}
		
		public ChunkPosition(int3 localPosition, int chunkSize)
		{
			LocalPosition = localPosition;
			ChunkSize = chunkSize;
		}
		
		public ChunkPosition(int x, int y, int z, int chunkSize)
		{
			ChunkSize = chunkSize;
			LocalPosition = new int3(x,y,z);
		}

		public override string ToString()
		{
			return LocalPosition.ToString();
			//return string.Format("int3({0},{1},{2})", X, Y, Z);
		}
	}
}