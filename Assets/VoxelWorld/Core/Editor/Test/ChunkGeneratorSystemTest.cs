﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using Unity.Entities;
using VoxelWorld.Core.PositionStructure;
using VoxelWorld.Core.Systems;

namespace VoxelWorld.Core.Editor.Test
{
	public class ChunkGeneratorSystemTest
	{
		private ChunkGeneratorSystem _chunkGeneratorSystem;


		[SetUp]
		public void Setup()
		{
			var chunkSystem = Substitute.For<IChunkSystem>();
			_chunkGeneratorSystem = new ChunkGeneratorSystem(chunkSystem);
		}

		[Test]
		public void GenerateChunk_SingleThread_Test()
		{
			int chunkSize = 16;
			var chunkPos = new ChunkPosition(1, 1, 1, chunkSize);

			var watch = new Stopwatch();
			watch.Start();
			var chunk = _chunkGeneratorSystem.GenerateChunk(chunkPos, new ChunkFactory(chunkSize));
			watch.Stop();
			UnityEngine.Debug.Log("1 chunk: "+watch.ElapsedMilliseconds);
			
			AssertThatChunkIsFilledWithStone(chunk, chunkSize);
		}

		[Test]
		public void GenerateChunks_SingleThread_StressTest()
		{
			int chunkSize = 16;
			List<ChunkPosition> positions = new List<ChunkPosition>();

			for (int i = 0; i < 1000; i++)
			{
				positions.Add(new ChunkPosition(i, i, i, chunkSize));
			}
			
			var result = GenerateChunksAndLog(positions,chunkSize);
			
			for (int i = 0; i < positions.Count; i++)
			{
				AssertThatChunkIsFilledWithStone(result[i], chunkSize);	
			}
		}
		
		[Test]
		public void GenerateChunks_MultiThreaded_StressTest()
		{
			
			int chunkSize = 16;
			List<ChunkPosition> positions = new List<ChunkPosition>();

			for (int i = 0; i < 1000; i++)
			{
				positions.Add(new ChunkPosition(i, i, i, chunkSize));
			}
			
			var watch = new Stopwatch();
			watch.Start();

			Task<List<Chunk>> task = _chunkGeneratorSystem.GenerateChunksMultiThreaded(positions, new ChunkFactory(chunkSize));
			task.Wait();
			
			watch.Stop();
			UnityEngine.Debug.Log(string.Format("{0} chunks in {1}ms, {2}ms per chunk.", positions.Count,
				watch.ElapsedMilliseconds, (watch.ElapsedMilliseconds *1f)/ (positions.Count*1f)));
			
			for (int i = 0; i < positions.Count; i++)
			{
				AssertThatChunkIsFilledWithStone(task.Result[i], chunkSize);	
			}
		}
		
		[Test]
		public void Generate6Chunks_SingleThread_Test()
		{
			int chunkSize = 16;
			List<ChunkPosition> positions = new List<ChunkPosition>()
			{
				new ChunkPosition(0, 1, 1, chunkSize),
				new ChunkPosition(1, 1, 1, chunkSize),
				new ChunkPosition(2, 1, 1, chunkSize),
				new ChunkPosition(3, 1, 1, chunkSize),
				new ChunkPosition(4, 1, 1, chunkSize),
				new ChunkPosition(5, 1, 1, chunkSize),
			};

			var result = GenerateChunksAndLog(positions,chunkSize);
			
			for (int i = 0; i < positions.Count; i++)
			{
				AssertThatChunkIsFilledWithStone(result[i], chunkSize);	
			}
		}

		private List<Chunk> GenerateChunksAndLog(List<ChunkPosition> positions, int chunkSize)
		{
			var watch = new Stopwatch();
			watch.Start();
			var result = _chunkGeneratorSystem.GenerateChunksSingleThreaded(positions, new ChunkFactory(chunkSize)).ToList();
			watch.Stop();
			UnityEngine.Debug.Log(string.Format("{0} chunks in {1}ms, {2}ms per chunk.", positions.Count,
				watch.ElapsedMilliseconds, (watch.ElapsedMilliseconds *1f)/ (positions.Count*1f)));
			return result;
		}
		
		[Test]
		public void JobExecuteTest()
		{
			var chunkSystem = Substitute.For<IChunkSystem>();
			var pos = new ChunkPosition(1, 1, 1, 16);
			int chunkSize = 16;

			ChunkGeneratorSystem.ChunkGeneratorJob job =
				new ChunkGeneratorSystem.ChunkGeneratorJob(pos, new ChunkFactory(chunkSize), chunkSystem);
			job.Execute();

			AssertThatChunkIsFilledWithStone(job.Chunk, chunkSize);
		}


		private void AssertThatChunkIsFilledWithStone(Chunk chunk, int chunkSize)
		{
			for (int x = 0; x < chunkSize; x++)
			{
				for (int y = 0; y < chunkSize; y++)
				{
					for (int z = 0; z < chunkSize; z++)
					{
						var voxelPos = new VoxelPosition(x, y, z, chunk);
						Assert.AreEqual("stone", chunk.GetVoxel(voxelPos).BlockType.GUID);
					}
				}
			}
		}
	}
}