﻿using System;
using System.Collections.Generic;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Assertions;
using VoxelWorld.Core.Interfaces;
using VoxelWorld.Core.PositionStructure;
using VoxelWorld.Core.Systems;
using Assert = NUnit.Framework.Assert;

namespace VoxelWorld.Core.Editor.Test
{
	public class ChunkSystemTest
	{
		private ChunkSystem _chunkSystem;
		private IChunkFactory<Chunk> _factory;

		[SetUp]
		public void Setup()
		{
			var chunkFactory = Substitute.For<IChunkFactory<Chunk>>();
			chunkFactory.CreateChunk(new ChunkPosition(1, 1, 1, 16)).Returns(new Chunk(new ChunkPosition(1,1,1,16), 16));
			
			_chunkSystem = new ChunkSystem(chunkFactory);
			_factory = chunkFactory;
		}
 
		[Test]
		public void SetAndGetChunkTest()
		{
			_chunkSystem.AddChunk(_factory.CreateChunk(new ChunkPosition(1, 1, 1, 16)));

			Assert.NotNull(_chunkSystem.GetChunk(new ChunkPosition(1, 1, 1, 16)));
		}

		[Test]
		public void GetChunkGlobalPositionTest()
		{
			Assert.Throws<KeyNotFoundException>(() => _chunkSystem.GetChunk(new GlobalPosition(20,20,20)));
			_chunkSystem.AddChunk(_factory.CreateChunk(new ChunkPosition(1, 1, 1, 16)));
			Assert.NotNull(_chunkSystem.GetChunk(new GlobalPosition(20,20,20)));
		}

		
		[Test]
		public void SetExistingChunk()
		{
			_chunkSystem.AddChunk(_factory.CreateChunk(new ChunkPosition(1, 1, 1, 16)));
			Assert.Throws<Exception>(() => _chunkSystem.AddChunk(_factory.CreateChunk(new ChunkPosition(1, 1, 1, 16))));
		}

		[Test]
		public void GetNullChunk()
		{
			Assert.Throws<KeyNotFoundException>(() => _chunkSystem.GetChunk(new ChunkPosition(1, 1, 1, 16)));
		}

	}
}