﻿using System;
using NSubstitute;
using NUnit.Framework;
using Unity.Mathematics;
using Unity.Transforms;
using VoxelWorld.Core.Interfaces;
using VoxelWorld.Core.PositionStructure;

namespace VoxelWorld.Core.Editor.Test
{
	public class VoxelPositionTest
	{
		private IChunk _chunk;

		[SetUp]
		public void Setup()
		{
			_chunk = Substitute.For<IChunk>();
			_chunk.chunkSize.Returns(16);
		}
		
		[Test]
		public void Int3Constructor()
		{
			var position = new VoxelPosition(new int3(1, 1, 1),_chunk);
			Assert.AreEqual(new int3(1,1,1), position.GlobalPosition.Value);
		}

		[Test]
		public void Int3ConstructorNullChunk()
		{
			Assert.Throws<ArgumentNullException>(() => new VoxelPosition(1, 1, 1, null));
		}
		
		[Test]
		public void ComponentConstructor()
		{	
			var position = new VoxelPosition(1,1,1,_chunk);
			Assert.AreEqual(new int3(1,1,1), position.GlobalPosition.Value);
		}
		
		[Test]
		public void ComponentConstructorNullChunk()
		{	
			Assert.Throws<ArgumentNullException>(() => new VoxelPosition(new int3(1,1,1), null));
		}
		
		[Test]
		public void GlobalToLocalInRangeTest()
		{
			var position = new VoxelPosition(1,1,1,_chunk);
			Assert.AreEqual(new int3(1,1,1), position.LocalPosition);
		}
		
		[Test]
		public void GlobalToLocalAtRangeTest()
		{
			var position = new VoxelPosition(15,15,15,_chunk);
			Assert.AreEqual(new int3(15,15,15), position.LocalPosition);
		}
		
		[Test]
		public void ConstructOverRangeTest()
		{
			Assert.Throws<ArgumentOutOfRangeException>(() => new VoxelPosition(16,16,16,_chunk));
		}
		
		[Test]
		public void ConstructUnderRangeTest()
		{
			Assert.Throws<ArgumentOutOfRangeException>(() =>new VoxelPosition(-1,-1,-1,_chunk));
		}

		[Test]
		public void IsPositionInRangeTest_InRange0()
		{
			var chunk = new Chunk(new ChunkPosition(0,0,0,16),16);
			
			Assert.DoesNotThrow(() => new VoxelPosition(new int3(0,0,0), chunk));
		}
		
		[Test]
		public void IsPositionInRangeTest_OverRange0()
		{
			var chunk = new Chunk(new ChunkPosition(0,0,0,16),16);
			
			Assert.Throws<ArgumentOutOfRangeException>(() => new VoxelPosition(new int3(16,16,16), chunk));
		}
		
		[Test]
		public void IsPositionInRangeTest_InRange1()
		{
			var chunk = new Chunk(new ChunkPosition(1,1,1,16),16);
			
			Assert.DoesNotThrow(() => new VoxelPosition(new int3(0,0,0), chunk));
		}
		
		[Test]
		public void IsPositionInRangeTest_UnderRange0()
		{
			var chunk = new Chunk(new ChunkPosition(1,1,1,16),16);
			
			Assert.Throws<ArgumentOutOfRangeException>(() => new VoxelPosition(new int3(-1,-1,-1), chunk));
		}

	}
}