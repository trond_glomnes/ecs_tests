﻿using NSubstitute;
using NUnit.Framework;
using Unity.Mathematics;
using VoxelWorld.Core.Interfaces;
using VoxelWorld.Core.PositionStructure;
using VoxelWorld.Core.Systems;

namespace VoxelWorld.Core.Editor.Test
{
	public class VoxelTest
	{
		private ChunkSystem _chunkSystem;
		private IBlockType _blockType;
		private IChunk _chunk;

		[SetUp]
		public void Setup()
		{
			var factorySubst = Substitute.For<IChunkFactory<Chunk>>();
			_chunkSystem = new ChunkSystem(factorySubst);
			_blockType = Substitute.For<IBlockType>();
			_chunk = Substitute.For<IChunk>();
			_chunk.chunkSize.Returns(16);
		}
		
		[Test]
		public void VoxelSetPosition()
		{
			var voxelPosition = new VoxelPosition(new int3(1,1,1),_chunk);
			Voxel voxel = new Voxel(voxelPosition,_blockType,_chunkSystem);
			Assert.AreEqual(voxelPosition, voxel.Position );
		}

		[Test]
		public void VoxelSetBlockType()
		{
			var voxelPosition = new VoxelPosition(new int3(1,1,1),_chunk);
			Voxel voxel = new Voxel(voxelPosition, _blockType,_chunkSystem);
			Assert.AreEqual(_blockType, voxel.BlockType);
		}
		
	}
}