﻿using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using Debug = UnityEngine.Debug;

namespace VoxelWorld.Core.Editor.Test
{
	public class AsyncTest
	{
		[Test]
		public void AwaitTest()
		{
			var watch = new Stopwatch();
			watch.Start();
			int iterations = int.MaxValue;
			var task = AsyncTestMethod(iterations);

			task.Wait();
			
			Assert.AreEqual(iterations,task.Result);
			
			watch.Stop();
			Debug.Log(watch.ElapsedMilliseconds);
		}

		private async Task<int> AsyncTestMethod(int iterations)
		{
			var a = await HeavyMethod(iterations);
			return a;
		}

		private async Task<int> HeavyMethod(int iterations)
		{
			int counter = 0;
			for (int i = 0; i < iterations; i++)
			{
				counter++;
			}

			return counter;
		}
	}
}