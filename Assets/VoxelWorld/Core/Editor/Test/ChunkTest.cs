﻿using System;
using NSubstitute;
using NUnit.Framework;
using Unity.Mathematics;
using VoxelWorld.Core.Interfaces;
using VoxelWorld.Core.PositionStructure;
using VoxelWorld.Core.Systems;

namespace VoxelWorld.Core.Editor.Test
{
	public class ChunkTest
	{
		private ChunkPosition _chunkPosition;
		private Chunk _chunk;

		[SetUp]
		public void Setup()
		{
			_chunkPosition = new ChunkPosition(new int3(1, 1, 1), 16);
			_chunk = new Chunk(_chunkPosition, 16);
		}

		[Test]
		public void ChunkSetPosition()
		{
			Assert.AreEqual(_chunkPosition, _chunk.ChunkPosition);
		}

		[Test]
		public void GetVoxelOverRangeTest()
		{
			Assert.Throws<ArgumentOutOfRangeException>(() => _chunk.GetVoxel(new VoxelPosition(100, 100, 100, _chunk)));
		}
		
		[Test]
		public void GetVoxelUnderRangeTest()
		{
			Assert.Throws<ArgumentOutOfRangeException>(() => _chunk.GetVoxel(new VoxelPosition(-100, -100, -100, _chunk)));
		}

		[Test]
		public void GetVoxelNotSetTest()
		{
			Assert.Throws<NullReferenceException>(() => _chunk.GetVoxel(new VoxelPosition(15, 15, 15, _chunk)));
		}
		
		[Test]
		public void GetVoxelOutOfRangeTest()
		{
			Assert.Throws<ArgumentOutOfRangeException>(() => _chunk.GetVoxel(new VoxelPosition(16, 16, 16, _chunk)));
		}

		[Test]
		public void SetAndGetVoxelTest()
		{
			var blockType = Substitute.For<IBlockType>();
			var chunkFactory = Substitute.For<IChunkFactory<Chunk>>();
			var chunkSystem = Substitute.For<ChunkSystem>(chunkFactory);

			var voxelPos = new VoxelPosition(15,15,15,_chunk);
			
			Assert.Throws<NullReferenceException>(() => _chunk.GetVoxel(voxelPos));

			var voxel = new Voxel(voxelPos, blockType, chunkSystem);
			_chunk.SetVoxel(voxel);

			Assert.AreEqual(voxel, _chunk.GetVoxel(voxelPos));
		}
	}
}