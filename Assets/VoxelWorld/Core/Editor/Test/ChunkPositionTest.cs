﻿using NUnit.Framework;
using Unity.Mathematics;
using VoxelWorld.Core.PositionStructure;

namespace VoxelWorld.Core.Editor.Test
{
	public class ChunkPositionTest
	{
		[Test]
		public void GlobalPositionConstructor1()
		{
			var position = new ChunkPosition(new GlobalPosition(1,1,1), 16);
			Assert.AreEqual(new int3(0,0,0), position.LocalPosition);
		}
		
		[Test]
		public void GlobalPositionConstructor15()
		{
			var position = new ChunkPosition(new GlobalPosition(15,15,15), 16);
			Assert.AreEqual(new int3(0,0,0), position.LocalPosition);
		}
		
		[Test]
		public void GlobalPositionConstructor16()
		{
			var position = new ChunkPosition(new GlobalPosition(16,16,16), 16);
			Assert.AreEqual(new int3(1,1,1), position.LocalPosition);
		}
		
		[Test]
		public void Int3Constructor()
		{
			var position = new ChunkPosition(new int3(1, 1, 1), 16);
			Assert.AreEqual(new int3(1, 1, 1), position.LocalPosition);
		}

		[Test]
		public void ComponentConstructor()
		{
			var position = new ChunkPosition(1, 1, 1, 16);
			Assert.AreEqual(new int3(1, 1, 1), position.LocalPosition);
		}

		[Test]
		public void LocalToGloball11()
		{
			var position = new ChunkPosition(1, 1, 1, 16);
			Assert.AreEqual(new int3(16, 16, 16), position.GlobalPosition);
		}
		
		[Test]
		public void LocalToGlobal222()
		{
			var position = new ChunkPosition(2, 2, 2, 16);
			Assert.AreEqual(new int3(32, 32, 32), position.GlobalPosition);
		}
		
		[Test]
		public void LocalToGlobal10()
		{
			var position = new ChunkPosition(10, 10, 10, 16);
			Assert.AreEqual(new int3(160, 160, 160), position.GlobalPosition);
		}
	}
}