﻿using UnityEngine;
using VoxelWorld.Core.Interfaces;

namespace VoxelWorld.Core
{
	public class BlockType : IBlockType
	{
		public BlockType(string guid)
		{
			GUID = guid;
		}
		
		public string GUID { get; }
	}
}