﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using VoxelWorld.Core.Interfaces;
using VoxelWorld.Core.PositionStructure;

namespace VoxelWorld.Core
{
	public struct Voxel : IVoxel {

		public IBlockType BlockType { get; }
		public VoxelPosition Position { get; }
		public IChunkSystem ChunkSystem { get; }
		
		public Voxel(VoxelPosition position, IBlockType blockType, IChunkSystem chunkSystem)
		{
			Position = position;
			ChunkSystem = chunkSystem;
			BlockType = blockType;
		}
		
		public IVoxel GetNeighbour(Direction direction)
		{
			var chunk = ChunkSystem.GetChunk(Position.GlobalPosition);
			return chunk.GetVoxel(Position+direction);
		}
	}
}
