﻿using VoxelWorld.Core.Interfaces;
using VoxelWorld.Core.PositionStructure;

namespace VoxelWorld.Core
{
	public class ChunkFactory : IChunkFactory<Chunk>
	{
		private readonly int _chunkSize;

		public ChunkFactory(int chunkSize)
		{
			_chunkSize = chunkSize;
		}

		public Chunk CreateChunk(ChunkPosition chunkPosition)
		{
			return new Chunk(chunkPosition, _chunkSize);
		}
	}
}