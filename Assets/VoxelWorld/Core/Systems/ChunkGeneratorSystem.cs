﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using VoxelWorld.Core.Interfaces;
using VoxelWorld.Core.PositionStructure;
using VoxelWorld.Core.Systems.Interfaces;

namespace VoxelWorld.Core.Systems
{
	public class ChunkGeneratorSystem : IChunkGeneratorSystem
	{
		private readonly IChunkSystem _chunkSystem;

		public ChunkGeneratorSystem(IChunkSystem chunkSystem)
		{
			_chunkSystem = chunkSystem;
		}

		//TODO figure out async result
		/*public void GenerateChunksSingleThreadedAsync(List<ChunkPosition> chunkPositions, IChunkFactory<Chunk> chunkFactory)
		{
			List<JobHandle> handles = new List<JobHandle>();
			List<ChunkGeneratorJob> jobs = new List<ChunkGeneratorJob>();

			JobHandle? prevHandle = null;
			foreach (var position in chunkPositions)
			{
				ChunkGeneratorJob job;
				var handle = GetJobHandle(position, chunkFactory, prevHandle, out job);
				handles.Add(handle);
				jobs.Add(job);
				prevHandle = handle;
			}
			
		}*/
		
		public IEnumerable<Chunk> GenerateChunksSingleThreaded(List<ChunkPosition> chunkPositions, IChunkFactory<Chunk> chunkFactory)
		{
			List<JobHandle> handles = new List<JobHandle>();
			List<ChunkGeneratorJob> jobs = new List<ChunkGeneratorJob>();

			JobHandle? prevHandle = null;
			foreach (var position in chunkPositions)
			{
				ChunkGeneratorJob job;
				var handle = GetJobHandle(position, chunkFactory, prevHandle, out job);
				handles.Add(handle);
				jobs.Add(job);
				prevHandle = handle;
			}
			prevHandle.Value.Complete();

			foreach (var job in jobs)
			{
				yield return job.Chunk;
			}
		}

		public async Task<List<Chunk>> GenerateChunksMultiThreaded(List<ChunkPosition> chunkPositions,
			IChunkFactory<Chunk> chunkFactory)
		{
			List<JobHandle> handles = new List<JobHandle>();
			List<ChunkGeneratorJob> jobs = new List<ChunkGeneratorJob>();

			foreach (var position in chunkPositions)
			{
				ChunkGeneratorJob job;
				var handle = GetJobHandle(position, chunkFactory, null, out job);
				handles.Add(handle);
				jobs.Add(job);
			}

			List<Chunk> chunks = new List<Chunk>();

			await Task.Run(() => WaitForHandlesTask(handles));
			
			for (var i = 0; i < handles.Count; ++i)
			{
				chunks.Add(jobs[i].Chunk);
			}

			return chunks;
		}

		private void WaitForHandlesTask(List<JobHandle> handles)
		{
			foreach (var handle in handles)
			{
				while (!handle.IsCompleted)
				{
					//wait...
				}
			}
		}
		
		public Chunk GenerateChunk(ChunkPosition chunkPosition, IChunkFactory<Chunk> chunkFactory)
		{
			ChunkGeneratorJob job;
			JobHandle handle = GetJobHandle(chunkPosition, chunkFactory,null, out job);
			handle.Complete();
			return job.Chunk;
		}

		private JobHandle GetJobHandle(ChunkPosition chunkPosition, IChunkFactory<Chunk> chunkFactory, JobHandle? dependency, out ChunkGeneratorJob job)
		{
			job = new ChunkGeneratorJob(chunkPosition, chunkFactory, _chunkSystem);
			var handle = dependency == null ? job.Schedule() : job.Schedule(dependency.Value);
			return handle;
		}
		
		//[ComputeJobOptimization]
		public struct ChunkGeneratorJob : IJob
		{
			private readonly IChunkSystem _chunkSystem;

			public Chunk Chunk { get; }

			public ChunkGeneratorJob(ChunkPosition chunkPosition, IChunkFactory<Chunk> chunkFactory,
				IChunkSystem chunkSystem)
			{
				_chunkSystem = chunkSystem;
				Chunk = chunkFactory.CreateChunk(chunkPosition);
			}

			public void Execute()
			{
				FillChunkWithVoxels();
			}

			private void FillChunkWithVoxels()
			{
				int iterationAmount = Chunk.chunkSize;
				//todo remove this
				//int3 cPos = Chunk.GlobalPosition.Value;

				for (int x = 0; x < iterationAmount; x++)
				{
					for (int y = 0; y < iterationAmount; y++)
					{
						for (int z = 0; z < iterationAmount; z++)
						{
							Chunk.SetVoxel(new Voxel(new VoxelPosition(x, y, z, Chunk),
								new BlockType("stone"),
								_chunkSystem));
						}
					}
				}
			}
		}
	}
}