﻿using VoxelWorld.Core;
using VoxelWorld.Core.Interfaces;
using VoxelWorld.Core.PositionStructure;

namespace Unity.Entities
{
	public interface IChunkSystem
	{
		IChunk GetChunk(ChunkPosition chunkPosition);
		IChunk GetChunk(GlobalPosition globalPosition);
		void AddChunk(IChunk chunk);
		IChunk CreateChunk(ChunkPosition chunkPosition);
	}
}