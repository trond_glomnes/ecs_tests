﻿using System;
using System.Collections.Generic;
using Unity.Entities;
using VoxelWorld.Core.Interfaces;
using VoxelWorld.Core.PositionStructure;

namespace VoxelWorld.Core.Systems
{
	public class ChunkSystem: IChunkSystem
	{
		public const int CHUNK_SIZE = 16;
		
		private readonly IChunkFactory<Chunk> _chunkFactory;
		private readonly Dictionary<ChunkPosition, Chunk> _chunks;

		public ChunkSystem(IChunkFactory<Chunk> chunkFactory)
		{
			_chunks = new Dictionary<ChunkPosition, Chunk>();
			_chunkFactory = chunkFactory;
		}

		public IChunk GetChunk(ChunkPosition chunkPosition)
		{
			return _chunks[chunkPosition];
		}

		public IChunk GetChunk(GlobalPosition globalPosition)
		{
			return _chunks[new ChunkPosition(globalPosition,CHUNK_SIZE)];
		}

		public void AddChunk(IChunk chunk)
		{
			if (_chunks.ContainsKey(chunk.ChunkPosition))
			{
				throw new Exception("Chunk already exists!");
			}

			if (!(chunk is Chunk))
			{
				throw new ArgumentException();
			}
			
			_chunks[chunk.ChunkPosition] = (Chunk) chunk;
		}

		public IChunk CreateChunk(ChunkPosition chunkPosition)
		{
			return _chunkFactory.CreateChunk(chunkPosition);
		}
	}
}