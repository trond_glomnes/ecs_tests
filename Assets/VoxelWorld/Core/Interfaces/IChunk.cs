﻿using Unity.Mathematics;
using VoxelWorld.Core.PositionStructure;

namespace VoxelWorld.Core.Interfaces
{
	public interface IChunk
	{
		ChunkPosition ChunkPosition { get; }
		GlobalPosition GlobalPosition { get; }
		int chunkSize { get; }
		void SetVoxel(IVoxel voxel);
		IVoxel GetVoxel(VoxelPosition globalPosition);
	}
}