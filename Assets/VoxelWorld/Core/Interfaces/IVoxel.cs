﻿using Unity.Mathematics;
using VoxelWorld.Core.PositionStructure;

namespace VoxelWorld.Core.Interfaces
{
	public interface IVoxel
	{
		VoxelPosition Position { get; }
		IBlockType BlockType { get; }
		IVoxel GetNeighbour(Direction direction);
	}
}