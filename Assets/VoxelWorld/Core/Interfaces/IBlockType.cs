﻿namespace VoxelWorld.Core.Interfaces
{
	public interface IBlockType
	{
		string GUID { get; }
	}
}