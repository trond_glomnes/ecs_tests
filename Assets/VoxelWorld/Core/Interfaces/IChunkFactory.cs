﻿using VoxelWorld.Core.PositionStructure;

namespace VoxelWorld.Core.Interfaces
{
	public interface IChunkFactory<T> where T:IChunk
	{
		T CreateChunk(ChunkPosition chunkPosition);
	}
}