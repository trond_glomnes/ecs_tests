﻿using System;
using VoxelWorld.Core.Interfaces;
using VoxelWorld.Core.PositionStructure;

namespace VoxelWorld.Core
{
	public struct Chunk:IChunk
	{
		private readonly IVoxel[,,] _voxels;
		
		public Chunk(ChunkPosition chunkPosition, int chunkSize)
		{
			ChunkPosition = chunkPosition;
			this.chunkSize = chunkSize;
			_voxels = new IVoxel[chunkSize,chunkSize,chunkSize];
		}

		public ChunkPosition ChunkPosition { get; }
		public GlobalPosition GlobalPosition
		{
			get { return ChunkPosition.GlobalPosition; }
		}
		public int chunkSize { get; }

		public void SetVoxel(IVoxel voxel)
		{
			var localPos = voxel.Position.LocalPosition;
			_voxels[localPos.x, localPos.y, localPos.z] = voxel;
		}

		public IVoxel GetVoxel(VoxelPosition voxelPosition)
		{
			var voxel = _voxels[voxelPosition.X, voxelPosition.Y, voxelPosition.Z];
			if(voxel == null) throw new NullReferenceException();
			return voxel;
		}

		
		
	}
}