﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VoxelWorld.Core;
using VoxelWorld.Core.PositionStructure;
using VoxelWorld.Core.Systems;

namespace VoxelWorld.Monobehaviours
{
	public class ChunkGenerationTester : MonoBehaviour
	{
		[SerializeField] private GameObject cube;
		private ChunkGeneratorSystem generator;

		private void Start()
		{
			generator = new ChunkGeneratorSystem(new ChunkSystem(new ChunkFactory(16)));
		}

		private void Update()
		{
			cube.transform.Rotate(Vector3.down, 1);

			if (Input.GetKeyUp(KeyCode.Alpha1))
			{
				int chunkSize = 16;
				List<ChunkPosition> positions = new List<ChunkPosition>();

				for (int i = 0; i < 1000; i++)
				{
					positions.Add(new ChunkPosition(i, i, i, chunkSize));
				}

				generator.GenerateChunksSingleThreaded(positions, new ChunkFactory(16)).ToList();
			}
			else if (Input.GetKeyUp(KeyCode.Alpha2))
			{
				int chunkSize = 16;
				List<ChunkPosition> positions = new List<ChunkPosition>();

				for (int i = 0; i < 1000; i++)
				{
					positions.Add(new ChunkPosition(i, i, i, chunkSize));
				}

				var task = generator.GenerateChunksMultiThreaded(positions, new ChunkFactory(16));
				task.Wait();
			}else if (Input.GetKeyUp(KeyCode.Alpha3))
			{
				//TODO remove
				/*int chunkSize = 16;
				List<ChunkPosition> positions = new List<ChunkPosition>();

				for (int i = 0; i < 1000; i++)
				{
					positions.Add(new ChunkPosition(i, i, i, chunkSize));
				}

				generator.GenerateChunksSingleThreadedAsync(positions, new ChunkFactory(16));
				*/
			}
		}
	}
}