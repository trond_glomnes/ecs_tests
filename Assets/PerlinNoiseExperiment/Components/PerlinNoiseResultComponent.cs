﻿using System;
using Unity.Collections;
using Unity.Entities;

namespace PerlinNoiseExperiment.Components
{	
	[Serializable]
	public struct PerlinNoiseResultComponent:IComponentData
	{
		
		
		//public NativeArray<float> Test;

		

		//public ResultData<float> Result;
		//public NativeArray<NativeArray<float>> TestResult; 
	}

	public struct ResultData<T> : ISharedComponentData where T : struct
	{
		public NativeArray<NativeArray<T>> FloatGrid;
	}
}