﻿using Unity.Entities;

namespace PerlinNoiseExperiment.Components
{
	public struct PerlinNoiseComponent:IComponentData
	{
		public float X;
		public float Y;
	}
}