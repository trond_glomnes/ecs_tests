﻿using Unity.Entities;
using UnityEngine;

namespace PerlinNoiseExperiment.Components
{
	[RequireComponent(typeof(MeshRenderer)),RequireComponent(typeof(MeshFilter))]
	public class PerlinNoiseResultComponentWrapper:ComponentDataWrapper<PerlinNoiseResultComponent>
	{
		private MeshRenderer _renderer;
		
		private void Start()
		{
			_renderer = GetComponent<MeshRenderer>();
			
		}

		private void LateUpdate()
		{
			/*var xSize = Value.Result.FloatGrid.Length;
			var ySize = Value.Result.FloatGrid[0].Length;

			var texture = _renderer.material.mainTexture as Texture2D;
			
			for (int x = 0; x < xSize; x++)
			{
				for (int y = 0; y < ySize; y++)
				{
					var value = Value.Result.FloatGrid[x][y];
					var color = new Color(value,value,value,1);
					texture.SetPixel(x,y,color);
				}
			}*/
		}
	}
}