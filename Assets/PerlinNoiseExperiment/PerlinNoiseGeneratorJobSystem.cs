﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace PerlinNoiseExperiment
{
	public class PerlinNoiseGeneratorJobSystem : JobComponentSystem
	{
		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			var job = new PerlinNoiseGenerationJob()
			{
				Direction = new float2(1, 1),
				Height = 1,
				Size = new int2(32, 32),
				Time = Time.timeSinceLevelLoad
			};
			
			var handle = job.Schedule(this, inputDeps);
			handle.Complete();
			return handle;
		}
	}
}