﻿using PerlinNoiseExperiment.Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace PerlinNoiseExperiment
{
	[ComputeJobOptimization]
	public struct PerlinNoiseGenerationJob : IJobProcessComponentData<PerlinNoiseComponent, PerlinNoiseResultComponent>
	{
		public float2 Direction;
		public float Time;
		public int2 Size;
		public float Height;

		public void Execute(ref PerlinNoiseComponent data, ref PerlinNoiseResultComponent result)
		{
			int xLength = Size.x;
			int yLength = Size.y;

			
			
			//var outer = new NativeArray<NativeArray<float>>(Size.x, Allocator.TempJob);

			/*for (int x = 0; x < xLength; x++)
			{
				var inner = new NativeArray<float>();
				for (int y = 0; y < yLength; y++)
				{
					inner[y] = Mathf.PerlinNoise((data.X + x + Direction.x * Time) * Height,
						(data.Y + y + Direction.y * Time) * Height);
				}

				outer[x] = inner;
				inner.Dispose();
			}

			//result.Result.FloatGrid = outer;
			outer.Dispose();*/
		}
	}
}