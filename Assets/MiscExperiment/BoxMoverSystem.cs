﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

namespace MiscExperiment
{
	public class BoxMoverSystem : JobComponentSystem
	{	
		[ComputeJobOptimization]
		private struct BoxMoverJob : IJobProcessComponentData<BoxMoverComponent, Position, TimeSinceStartupComponent>
		{
			public void Execute(ref BoxMoverComponent data, ref Position position, ref TimeSinceStartupComponent timeSinceStartup)
			{
				position.Value.y = math.sin(timeSinceStartup.Value *data.WaveSpeed+ position.Value.x*data.WaveSize)*data.WaveHeight +
				                   math.sin(timeSinceStartup.Value * data.WaveSpeed + position.Value.z*data.WaveSize)*data.WaveHeight;
			}
		}

		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			return new BoxMoverJob().Schedule(this, 64, inputDeps);
		}
	}
	
	public struct BoxMoverComponent:IComponentData
	{
		public float WaveSize;
		public float WaveHeight;
		public float WaveSpeed;
	}
}