﻿using Unity.Entities;
using Unity.Jobs;
using UnityEngine;

namespace MiscExperiment
{
	public class DeltaTimeSystem:JobComponentSystem
	{
		struct DeltaTimeJob:IJobProcessComponentData<DeltaTime>
		{
			public float DeltaTime;
			
			public void Execute(ref DeltaTime data)
			{
				data.Value = DeltaTime;
			}
		}
		
		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			var job = new DeltaTimeJob { DeltaTime = Time.deltaTime};
			var handler = job.Schedule(this, 64, inputDeps);
			return handler;
		}
	}
}