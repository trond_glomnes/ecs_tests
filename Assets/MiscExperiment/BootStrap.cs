﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Rendering;

namespace MiscExperiment
{
	public class BootStrap : MonoBehaviour
	{
		public int3 Size;
		public float ObjectDistance;

		public float WaveSize;
		public float WaveHeight;
		public float WaveSpeed;

		public Mesh Mesh;
		public Material Material;

		private void Start()
		{
			var entityManager = World.Active.GetOrCreateManager<EntityManager>();

			for (int x = 0; x < Size.x; x++)
			{
				for (int y = 0; y < Size.y; y++)
				{
					for (int z = 0; z < Size.z; z++)
					{
						var testEntity = entityManager.CreateEntity(ComponentType.Create<BoxMoverComponent>(),
							ComponentType.Create<Position>(),
							ComponentType.Create<TransformMatrix>(),
							ComponentType.Create<MeshInstanceRenderer>(),
							ComponentType.Create<TimeSinceStartupComponent>());

						entityManager.SetSharedComponentData(testEntity,
							new MeshInstanceRenderer
							{
								material = Material,
								mesh = Mesh,
								receiveShadows = true,
								castShadows = ShadowCastingMode.On
							});

						entityManager.SetComponentData(testEntity, new Position
						{
							Value = new float3
							{
								x = x * ObjectDistance,
								y = y * ObjectDistance,
								z = z * ObjectDistance
							}
						});

						entityManager.SetComponentData(testEntity, new BoxMoverComponent()
						{
							WaveSize = WaveSize,
							WaveHeight = WaveHeight,
							WaveSpeed = WaveSpeed
						});
					}
				}
			}
		}
	}
}