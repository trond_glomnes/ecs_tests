﻿using System.ComponentModel;
using Unity.Entities;

namespace MiscExperiment
{
	public struct DeltaTime:IComponentData
	{
		public float Value;
	}
}