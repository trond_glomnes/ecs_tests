﻿using Unity.Entities;
using Unity.Jobs;
using UnityEngine;

namespace MiscExperiment
{
	public class TimeSinceStartupSystem:JobComponentSystem
	{
		[ComputeJobOptimization]
		private struct TimeSinceStartupJob:IJobProcessComponentData<TimeSinceStartupComponent>
		{
			public float TimeSinceStartup;
			
			public void Execute(ref TimeSinceStartupComponent data)
			{
				data.Value = TimeSinceStartup;
			}
		}

		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			return new TimeSinceStartupJob {TimeSinceStartup = Time.realtimeSinceStartup}.Schedule(this,64,inputDeps);
		}
	}
	
	public struct TimeSinceStartupComponent:IComponentData
	{
		public float Value;
	}
}