﻿using NUnit.Framework;
using PureECS;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace DefaultNamespace
{
	public class MiscTest
	{
		[Test]
		public void GetIndexFrom2DTest()
		{
			var size = new int2(2,2);

			int2 input;
			int result;
			
			input = new int2(0,0);
			result = Array2DConverter.GetIndexFrom2D(size, input);
			Assert.AreEqual(0,result);
			
			input = new int2(1,0);
			result = Array2DConverter.GetIndexFrom2D(size, input);
			Assert.AreEqual(1,result);
			
			input = new int2(0,1);
			result = Array2DConverter.GetIndexFrom2D(size, input);
			Assert.AreEqual(2,result);
			
			input = new int2(1,1);
			result = Array2DConverter.GetIndexFrom2D(size, input);
			Assert.AreEqual(3,result);
		}
		
		[Test]
		public void Get2DFromIndexTest()
		{
			var size = new int2(2,2);

			int2 result;
			
			result = Array2DConverter.Get2DFromIndex(size, 0);
			Assert.AreEqual(new int2(0,0), result);
			
			result = Array2DConverter.GetIndexFrom2D(size, 1);
			Assert.AreEqual(new int2(1,0),result);
			
			result = Array2DConverter.GetIndexFrom2D(size, 2);
			Assert.AreEqual(new int2(0,1),result);
			
			result = Array2DConverter.GetIndexFrom2D(size, 3);
			Assert.AreEqual(new int2(1,1),result);
		}

	}
}