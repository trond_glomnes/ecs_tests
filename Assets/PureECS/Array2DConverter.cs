﻿using Unity.Collections;
using Unity.Mathematics;

namespace PureECS
{
	public class Array2DConverter
	{
		public static int GetIndexFrom2D(int2 arraySize, int2 input)
		{
			return arraySize.y * input.y + input.x;
		}

		public static int2 Get2DFromIndex(int2 arraySize, int input)
		{
			var result = new int2(input/arraySize.y, input%arraySize.x);
			return result;
		}
		
		public static T GetValue2D<T>(NativeArray<T> nativeArray, int2 arraySize, int2 input) where T : struct
		{
			return nativeArray[GetIndexFrom2D(arraySize,input)];
		}

		public static void SetValue2D<T>(NativeArray<T> nativeArray, int2 arraySize, int2 input) where T : struct
		{
			
		}
	}
}