﻿using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Timeline;

namespace PureECS
{
	public class TestSystem:MonoBehaviour
	{
		[SerializeField]private Texture2D _texture2D;
		[SerializeField]private int2 _textureSize;
		
		private NativeArray<NativeArray<float>> _heightMap;
		private NativeArray<Color> _colorArray;
		
		private void Start()
		{
			_heightMap = new NativeArray<NativeArray<float>>(_textureSize.x,Allocator.Persistent);
			
			for (int x = 0; x < _textureSize.x; x++)
			{
				_heightMap[x] = new NativeArray<float>(_textureSize.y,Allocator.Persistent);
				for (int y = 0; y < _textureSize.y; y++)
				{
					var a = _heightMap[x];
					a[y] = 1;
					_heightMap[x] = a;
				}
			}
		}

		private void OnDestroy()
		{
			_heightMap.Dispose();
			_colorArray.Dispose();
		}

		private void Update()
		{
			
		}

		private void LateUpdate()
		{
			for (int x = 0; x < _textureSize.x; x++)
			{
				for (int y = 0; y < _textureSize.y; y++)
				{
					
				}
			}
		}

		
		
		public class TestJob:IJob
		{
			public void Execute()
			{
				
			}
		}

		public class TestJobParallelFor:IJobParallelFor
		{
			public void Execute(int index)
			{
				
			}
		}
		
		public class TestJobParallelForBatch:IJobParallelForBatch
		{
			public void Execute(int startIndex, int count)
			{
				
			}
		}

		
	}
}